#include "IRQHandler.h"
#include "stm32f10x.h"
#include "stm32f10x_exti.h"
#include "Dcf77.h"
#include "serial2.h"
#include "pc13led.h"
//#include "Led.h"
#include "bluepill.h"

#include <stdio.h>
#include <stdlib.h>

extern uint32_t milliseconds_after_reset;
extern Dcf77	clock_signal_receiver;
extern char temp_str[100];


void TIM4_IRQHandler(void) {

	TIM_ClearITPendingBit(TIM4, TIM_IT_Update);

	milliseconds_after_reset++;

} //TIM4_IRQHandler


void EXTI0_IRQHandler(void) {

#define UNDEFINED	2

	static uint32_t	previous_rising_edge_time_stamp;
	uint32_t		current_edge_time_stamp;
	uint16_t		time_after_previous_rising_edge;
	uint8_t			received_bit = UNDEFINED;
	uint8_t			pin_status;

	EXTI_ClearITPendingBit(EXTI_Line0);

	current_edge_time_stamp = millis();
	time_after_previous_rising_edge = current_edge_time_stamp - previous_rising_edge_time_stamp;

	if (time_after_previous_rising_edge < (BIT_0_LENGTH_MS - BIT_LENGTH_TOLERANCE_MS) ) {
		return;
	}

	pin_status = clock_signal_receiver.get_signal_state();
	pc13led_set(pin_status);

	previous_rising_edge_time_stamp = current_edge_time_stamp;

	sprintf(temp_str, "[%u:%u] - ", pin_status, time_after_previous_rising_edge);
	serial2_send(temp_str);

	if (pin_status == HIGH) {

		if (time_after_previous_rising_edge < (BIT_PERIOD_MS - BIT_PERIOD_TOLERANCE_MS) ) {
			serial2_send("rising edge too early...<\n");
			clock_signal_receiver.current_buffer_bit = 0;
			return;
		} else if (time_after_previous_rising_edge > (BIT_PERIOD_MS + BIT_PERIOD_TOLERANCE_MS) ) {
			serial2_send("SOF detected<\n");
			clock_signal_receiver.current_buffer_bit = 0;
			return;
		}
	}

	if (pin_status == LOW) {

		if (	(time_after_previous_rising_edge > BIT_0_LENGTH_MS - BIT_LENGTH_TOLERANCE_MS) &&
				(time_after_previous_rising_edge < BIT_0_LENGTH_MS + BIT_LENGTH_TOLERANCE_MS)	) {
			serial2_send("bit 0 ");
			received_bit = 0;
		}

		else if (	(time_after_previous_rising_edge > BIT_1_LENGTH_MS - BIT_LENGTH_TOLERANCE_MS) &&
					(time_after_previous_rising_edge < BIT_1_LENGTH_MS + BIT_LENGTH_TOLERANCE_MS)	) {
			serial2_send("bit 1 ");
			received_bit = 1;
		}

		if (received_bit != UNDEFINED) {
			clock_signal_receiver.buffer[clock_signal_receiver.current_buffer_bit] = received_bit;
			clock_signal_receiver.current_buffer_bit++;
			if (clock_signal_receiver.current_buffer_bit > 58) {
				//clock_signal_receiver.frame = clock_signal_receiver.buffer;
				serial2_send("frame ready!");
				clock_signal_receiver.update_current_time_from_buffer();
				clock_signal_receiver.current_buffer_bit = 0;
			}
		}
			serial2_send("\n");
			return;

	}

} //EXTI0_IRQHandler();


void EXTI15_10_IRQHandler(void) {
} //EXTI15_10_IRQHandler();
