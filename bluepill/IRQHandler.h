#ifndef IRQHANDLER_H_
#define IRQHANDLER_H_

extern "C" void TIM4_IRQHandler(void);
extern "C" void EXTI0_IRQHandler(void);
extern "C" void EXTI15_10_IRQHandler(void);

#endif /* IRQHANDLER_H_ */
