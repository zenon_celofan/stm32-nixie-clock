#include <DS3231.h>

DS3231::DS3231() {

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	 GPIO_InitTypeDef GPIO_InitStructure;
	 I2C_InitTypeDef i2c;

	 RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_GPIOC | RCC_APB2Periph_GPIOD, ENABLE);



	 GPIO_StructInit(&gpio);
	 gpio.GPIO_Pin = GPIO_Pin_6|GPIO_Pin_7; // SCL, SDA
	 gpio.GPIO_Mode = GPIO_Mode_AF_OD;
	 gpio.GPIO_Speed = GPIO_Speed_50MHz;
	 GPIO_Init(GPIOB, &gpio);

	 I2C_StructInit(&i2c);
	 i2c.I2C_Mode = I2C_Mode_I2C;
	 i2c.I2C_ClockSpeed = 100000;
	 I2C_Init(I2C1, &i2c);
	 I2C_Cmd(I2C1, ENABLE);
}

