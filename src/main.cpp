#include <stdio.h>
#include <stdlib.h>
#include "stm32f10x.h"
#include "bluepill.h"
#include "delay.h"
#include "Dcf77.h"
#include "serial2.h"
#include "pc13led.h"
#include "IRQHandler.h"

#include "stm32f10x_tim.h"


Dcf77 clock_signal_receiver(PA0, PA1);
uint32_t time = 0;
char temp_str[100];


void main() {

	millis_init();

	pc13led_init();

	serial2_init();			//debug serial
	serial2_send("\n\n\n*** nixie_clock - RESET ***\n\n");

    time = millis();


	while (1) {



		sprintf(temp_str, "\n*** %u%u:%u%u ***\n", (clock_signal_receiver.current_time & 0b1111000000000000) >> 12, (clock_signal_receiver.current_time & 0b0000111100000000) >> 8, (clock_signal_receiver.current_time & 0b0000000011110000) >> 4, (clock_signal_receiver.current_time & 0b1111));
		serial2_send(temp_str);
		delay(10000);
	}
} //main()


