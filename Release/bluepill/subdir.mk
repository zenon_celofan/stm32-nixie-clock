################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../bluepill/IRQHandler.cpp \
../bluepill/Led.cpp \
../bluepill/bluepill.cpp \
../bluepill/delay.cpp \
../bluepill/pc13led.cpp \
../bluepill/serial2.cpp 

OBJS += \
./bluepill/IRQHandler.o \
./bluepill/Led.o \
./bluepill/bluepill.o \
./bluepill/delay.o \
./bluepill/pc13led.o \
./bluepill/serial2.o 

CPP_DEPS += \
./bluepill/IRQHandler.d \
./bluepill/Led.d \
./bluepill/bluepill.d \
./bluepill/delay.d \
./bluepill/pc13led.d \
./bluepill/serial2.d 


# Each subdirectory must supply rules for building sources it contributes
bluepill/%.o: ../bluepill/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GNU ARM Cross C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -fsigned-char -ffunction-sections -fdata-sections -ffreestanding -Wall -Wextra  -g -DNDEBUG -DSTM32F10X_MD -DUSE_STDPERIPH_DRIVER -DHSE_VALUE=8000000 -I"../include" -I"../system/include" -I"../system/include/cmsis" -I"../system/include/stm32f1-stdperiph" -I"H:\STM32ToolChain\Workspace\nixie_clock\bluepill" -I"H:\STM32ToolChain\Workspace\nixie_clock\Dcf77" -std=gnu++11 -fabi-version=0 -fno-exceptions -fno-rtti -fno-use-cxa-atexit -fno-threadsafe-statics -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


