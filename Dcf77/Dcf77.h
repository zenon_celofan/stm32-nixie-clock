#ifndef DCF77_H_
#define DCF77_H_

#include "stm32f10x.h"
#include "stm32f10x_gpio.h"


#define	 BIT_PERIOD_MS				1000
#define	 BIT_PERIOD_TOLERANCE_MS	500
#define  BIT_0_LENGTH_MS			100
#define	 BIT_1_LENGTH_MS			200
#define	 BIT_LENGTH_TOLERANCE_MS	49

#define	 DEFAULT_MINUTES_1			0
#define	 DEFAULT_MINUTES_10			0
#define	 DEFAULT_HOURS_1			2
#define	 DEFAULT_HOURS_10			1


class Dcf77 {

public:

	uint8_t		buffer[59];
	uint8_t 	current_buffer_bit;
	uint8_t		time_frame[59];
	uint16_t	current_time;

			Dcf77(uint8_t tn_pin, uint8_t po_pin);
	void	turn_on(void);
	void	turn_off(void);
	uint8_t get_signal_state(void);
	void	update_current_time_from_buffer(void);


private:

	GPIO_TypeDef * 	tn_pin_port;
	uint16_t 		tn_pin_number;

	GPIO_TypeDef * 	po_pin_port;
	uint16_t		po_pin_number;


	void	init_pins(void);
	void	init_interrupt(void);
};



#endif
