#include "Dcf77.h"
#include "stm32f10x.h"
#include "stm32f10x_gpio.h"
#include "stm32f10x_exti.h"
#include "bluepill.h"
#include "delay.h"

Dcf77::Dcf77(uint8_t tn_pin, uint8_t po_pin) {

	tn_pin_port = get_pin_port(tn_pin);
	tn_pin_number = get_pin_number(tn_pin);

	po_pin_port = get_pin_port(po_pin);
	po_pin_number = get_pin_number(po_pin);

	current_time = (DEFAULT_HOURS_10 << 24) | (DEFAULT_HOURS_1 << 16) | (DEFAULT_MINUTES_10 << 8) | (DEFAULT_MINUTES_1);

	uint8_t current_buffer_bit;


	init_pins();
	init_interrupt();

	//turn_off();
	//delay(100);
	turn_on();

} //Dcf77


void Dcf77::turn_on(void) {
	GPIO_WriteBit(po_pin_port, po_pin_number, Bit_RESET);
}


void Dcf77::turn_off(void) {
	GPIO_WriteBit(po_pin_port, po_pin_number, Bit_SET);
}


uint8_t Dcf77::get_signal_state(void) {

	uint8_t signal_state;

	signal_state = GPIO_ReadInputDataBit(tn_pin_port, tn_pin_number);

	return signal_state;

} //get_signal_state()


void	Dcf77::init_pins(void) {

	GPIO_InitTypeDef  GPIO_InitStructure;

	//TN
	if (tn_pin_port == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	} else if (tn_pin_port == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	} else if (tn_pin_port == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}
	GPIO_InitStructure.GPIO_Pin = tn_pin_number;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING; //GPIO_Mode_IPU;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(tn_pin_port, &GPIO_InitStructure);


	//PO
	if (po_pin_port == GPIOA) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	} else if (po_pin_port == GPIOB) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB, ENABLE);
	} else if (po_pin_port == GPIOC) {
		RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC, ENABLE);
	}
	GPIO_InitStructure.GPIO_Pin = po_pin_number;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(po_pin_port, &GPIO_InitStructure);
}


void	Dcf77::init_interrupt(void) {

	uint8_t interrupt_source_pin_port;
	uint8_t interrupt_source_pin_number;


	if (tn_pin_port == GPIOA) {
		interrupt_source_pin_port = GPIO_PortSourceGPIOA;
	} else if (tn_pin_port == GPIOB) {
		interrupt_source_pin_port = GPIO_PortSourceGPIOB;
	} else if (tn_pin_port == GPIOC) {
		interrupt_source_pin_port = GPIO_PortSourceGPIOC;
	}

	if (tn_pin_number == 0) {
		interrupt_source_pin_number = GPIO_PinSource0;
	} else if (tn_pin_number == 1) {
		interrupt_source_pin_number = GPIO_PinSource1;
	}

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);

	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA, GPIO_PinSource0);
	EXTI_InitTypeDef   EXTI_InitStructure;
	EXTI_InitStructure.EXTI_Line = EXTI_Line0;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	NVIC_InitTypeDef   NVIC_InitStructure;
    NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn;
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}


void	Dcf77::update_current_time_from_buffer(void) {

	uint8_t		minutes = 0;
	uint8_t		hours = 0;

	minutes = buffer[21] | (buffer[22] << 1) | (buffer[23] << 2) | (buffer[24] << 3) | (buffer[25] << 4) | (buffer[26] << 5) | (buffer[27] << 6);
	if (check_parity((uint16_t) minutes) != buffer[28]) {
		return;
	}

	hours = buffer[29] | (buffer[30] << 1) | (buffer[31] << 2) | (buffer[32] << 3) | (buffer[33] << 4) | (buffer[34] << 5);
	if (check_parity((uint16_t) hours) != buffer[35]) {
		return;
	}

	current_time = (hours << 8) | minutes;
}
